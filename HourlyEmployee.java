import java.io.*;
public class HourlyEmployee extends Employee implements Serializable
{
	
	public HourlyEmployee(String name, double wage)
	{
		super(name, wage);
	}
	
	public String computePay(int hours)
	{
		double pay;
		int tHalf;
		double tempWage;
		if(hours > 40)
		{
			tHalf = hours - 40;
			tempWage = wage * 1.5;
			pay = (wage * 40) + (tHalf * tempWage);
		}
		else
			pay = hours * wage;
		return Utilities.toDollars(pay);
	}

	public String toString()
	{
		String result = "";
		int length;
		String tempWage = Utilities.toDollars(wage);
		result += name;
		length = 33 - name.length() - tempWage.length(); 
		for(int i = 0; i < length; i++)
			result += " ";
		result += ("$" + tempWage + "/hour");
		return result;
	}

}


