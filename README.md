
//Gideon Ojo
//CSC 205 Project #3
//	The Personal Database

There will be 4 classes used for the database program 
PDB (Main):
	Where the main program is going to be run. 
	Will include static methods for specific functions
Employee (Abstract):
	-Outline for employees. 
	Contains 2 Getters(Name[String], Wage[double]) and 2 Setters(Name, Wage)
		All return void
	Method to increase wage (double).
	Abstract Method to compute weekly pay(double)
HourlyEmployee (Extends Employee):
	compute pay 
	toString
SalariedEmployee (Extends Employee):
	getSalary && setSalary
	compute pay
	toString

Main method will use inheritance by first creating an employee then later defining them as 
a Hourly or Salaried employee

Steps:
1. Create Employee class. Give employee attributes
2. Create Child classes (HOurly and Salaried)
3. Create Main database class
	1. Begin by crating the menu (use switch/case) 
		*Make sure it is redisplayed after each command is executed
	2. Next add method response to each case
	3. Customize methods with user input, add to the database and more. 
Make sure to add comments and test algorithm

