import java.io.*;
public class SalariedEmployee extends Employee implements Serializable
{
	private double salary;

        public SalariedEmployee(String name, double salary)
        {
                super(name, (salary / 52));
		this.salary = salary; 
        }

        public String computePay(int hours)
        {
                double pay = wage;
                return Utilities.toDollars(pay);
        }
	
	public double getSalary()
	{
		return wage * 52;
	}

	public void setSalary(double salary)
	{
		wage = salary / 52.00;
	}
	
        public String toString()
        {
                String result = "";
                int length;
                String tempWage = Utilities.toDollars(wage * 52.00);
                result += name;
                length = 33 - name.length() - tempWage.length();
                for(int i = 0; i < length; i++)
                        result += " ";
                result += ("$" + tempWage + "/year");
                return result;
        }

}

