import java.io.*;
public abstract class Employee implements Serializable
{
	protected String name;
	protected double wage;

	protected Employee(String name, double wage)
	{
		this.name = name;
		this.wage = wage;
	}

	public abstract String computePay(int hours);

	public String getName()
	{
		//Retuen the Name
		return name;
	}
	public double getWage()
	{
		//Return the wage
		return wage;
	}
	public void setName(String name)
	{
		//Reset the name
		this.name = name;
	}
	public void setWage(double wage)
	{
		//Reset the wage
		this.wage = wage;
	}

	public void increaseWage(double increase)
	{
		//Increase the Employee wage by the percentage specified 
		wage = ((increase / 100.0) + 1) * wage;
	}

}
