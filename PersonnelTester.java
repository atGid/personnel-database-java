public class PersonnelTester
{

        public static void main (String [] args)
        {
                Employee a = new HourlyEmployee("Munsey, Scotty", 20.00);
                Employee b = new SalariedEmployee("Ojo, Gideon", 75000);    //*****//

                System.out.println(a);
                System.out.println(b);

                a.setWage(25.00);
                b.setWage(8500);

                System.out.println(a);
                System.out.println(b);

                a.increaseWage(2.5);
                b.increaseWage(2.5);

                System.out.println("New wage a: " + a.getWage());
                System.out.println("New wage b: " + b.getWage());
                //System.out.println("New salary b: " + b.getSalary());
                System.out.println();
                System.out.println(a);
                System.out.println(b);

                System.out.println("30 hours a = " + a.computePay(30));
                System.out.println("50 hours a = " + a.computePay(50));
                System.out.println("10 hours b = " + b.computePay(10));
                System.out.println("Infinite hours b = " + b.computePay(5000));

        }

}

