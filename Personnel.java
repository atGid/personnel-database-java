import java.util.*;
import java.io.*;
/*
 *	Gideon Ojo. 
 *	CSC 205 - Digh
 *	Project 3
 */
public class Personnel
{

	public static void main (String [] args)
	{
		//Main
		ArrayList<Employee> employees = new ArrayList<Employee>(); //Add ArrayList to store Employees
		clearScreen();
		menu(employees); //Run the main Program
	}

	public static void printMenu()
	{
		//Print the main menu with this specific format
		for(int i = 0; i < 40; i++)
		System.out.print("/");
		System.out.println("\n" + "//" + "Commands:" + "\t" + " n - New employee");
		System.out.println("//" + "\t" + "\t" + " c - Compute Paychecks");
		System.out.println("//" + "\t" + "\t" + " r - Raise wages");
		System.out.println("//" + "\t" + "\t" + " p - Print records");
		System.out.println("//" + "\t" + "\t" + " d - Download data");
		System.out.println("//" + "\t" + "\t" + " u - Upload data");
		System.out.println("//" + "\t" + "\t" + " f - Find Employee");
		System.out.println("//" + "\t" + "\t" + " q - Quit");
		for(int i = 0; i < 40; i++)
			System.out.print("/");
		System.out.println();
		System.out.print("Enter Command: ");
	}

	public static void menu(ArrayList<Employee> employee)
	{
		printMenu();
		Scanner in = new Scanner(System.in);
		char option = 'a';
		boolean run = true; 
		//Loop runs until user quits
		while(run)
		{
			option = in.next().charAt(0);//Take in user character
			//Switch calls specific methods for each function of the program
			switch(option)
			{
				case 'n':
				case 'N':
					addEmployee(employee);
					printMenu();
				break;
				case 'c':
				case 'C':
					computePaycheck(employee);
					printMenu();
				break;
				case 'r':
				case 'R':
					raiseWages(employee);
					printMenu();
				break;
				case 'p':
				case 'P':
					printEmployees(employee);
					printMenu();
				break;
				case 'd':
				case 'D':
					download(employee);
					printMenu();
				break;
				case 'u':
				case 'U':
					upload(employee);
					printMenu();
				break;
				case 'f':
				case 'F':
					sortEmployees(employee);
					int location = employeeLocation(employee);
					if(location >= 0)
						System.out.println(employee.get(location));
					else
						System.out.println("Sorry, this employee was not found.");
					printMenu();
				break;
				case 'q':
				case 'Q':
					run = false;
				break;
				default: System.out.println("Command not recognized; please try again.");
					System.out.print("Enter Command: ");
				break;
			}
		}
	}

	private static int employeeLocation(ArrayList<Employee> employee)
	{
		//method takes Arraylist as a parameter
		//returns the position of item in array
		//Binray search. -1 is returned if name not found
		int element = 0; String name = "";
		System.out.print("Enter Employee name: ");
		Scanner in = new Scanner(System.in);
		name = in.nextLine();
		
		int first = 0; 
		int last = employee.size() - 1;
		int  middle;
		boolean found = false;
		
		do
		{
			middle = (first + last) / 2;
			if((employee.get(middle)).getName().compareTo(name) == 0)
				found = true;
			else if(employee.get(middle).getName().compareTo(name) > 0)
				last = middle - 1;
			else
				first = middle + 1;

		} while( (!found) && (first <= last) );
			
		element = middle;
		
		return (found ? element : -1);
	}

	private static void sortEmployees(ArrayList<Employee> employee)
	{
		//Method is void. Parameter is employee ArrayList
		//Sorts the objects in ascending order
		int minIndex, index, j;
		Employee temp;

		for(index = 0; index < employee.size() - 1; index++)
		{
			minIndex = index;
			for(j = minIndex+1; j < employee.size(); j++)
			{
				if(employee.get(j).getName().compareTo(employee.get(minIndex).getName()) < 0)
					minIndex = j;
			}
			if(minIndex != index)
			{
				temp = employee.get(index);
				employee.set(index, employee.get(minIndex));
				employee.set(minIndex, temp);
			}
		}
	}

	private static void upload(ArrayList<Employee> employee)
	{
		//Method upload is void. Parameter is ArrayList of employee
		//uploads data from arraylist to an external file
		String fileName = "Employees.dat";
		
		try {
			FileOutputStream fileOut = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(employee);
			out.close();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static void download(ArrayList<Employee> employee)
	{
		//Method download is void. Parameter is ArrayList of employees
		//Downloads data from fileName within the directory
		String fileName = "Employees.dat";
		employee.clear();
		ArrayList<Employee> temp  = new ArrayList<Employee>();
                try {
                        FileInputStream fileIn = new FileInputStream(fileName);
                        ObjectInputStream in = new ObjectInputStream(fileIn);
                        temp = (ArrayList<Employee>) in.readObject();
                        in.close();
                }
                catch (IOException e) {
                        System.out.println(e.getMessage());
                }
                catch (ClassNotFoundException e)
                {
                        System.out.println(e.getMessage());
                }
                for(int i = 0; i < temp.size(); i++)
                        employee.add(temp.get(i));
	}

	private static void raiseWages(ArrayList<Employee> employee)
	{
		//Method raiseWages is void. Parameter is ArrayList of employee
		//Takes user input of wage increase and applies it
		//	to each employee in the arraylist
		Scanner in = new Scanner(System.in);
		System.out.print("Enter percentage increase:  ");
		double increase = in.nextDouble();
		System.out.println("\n" + "New Wages");
		System.out.println("~~~~~~~~~");
		for(int i = 0; i < employee.size(); i++)
		{
			employee.get(i).increaseWage(increase);
		}
		printEmployees(employee);
	}

	private static void computePaycheck(ArrayList<Employee> employee)
	{
		//Method computePaycheck is void. Paramater is ArrayList of employees
		//Takes user input as to the amount of hours worked and creates a 
		//	paycheck
		Scanner in = new Scanner(System.in);
		for(int i = 0; i < employee.size(); i++)
		{
			System.out.print("Enter the number of hours worked by " + employee.get(i).getName() + ": ");
			int hours = in.nextInt();
			System.out.println("Pay:  $" + employee.get(i).computePay(hours));
		}
	}

	private static void printEmployees(ArrayList<Employee> employee)
	{
		//Method printEmployees is void. Takes an ArrayList of employee as a paramater
		//Prints each employees name and their wage or salary 
		for(int i = 0; i < employee.size(); i++)
			System.out.println(employee.get(i));
	}

	private static void addEmployee(ArrayList<Employee> employee)
	{
		//Method is void. ArrayList of employees is the parameter
		//Adds an Employee object to the arraylist
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the name of the new employee: ");
		String name = in.nextLine();
		System.out.print("Hourly (h) or Salaried (s): ");
		char choice = in.next().charAt(0);
		switch(choice)
		{
			case 'h':
			case 'H':
				System.out.print("Enter hourly wage: ");
				double wage = in.nextDouble();
				employee.add(new HourlyEmployee(name, wage));
			break;
			case 's':
			case 'S':
				System.out.print("Enter annual salary: ");
				double salary = in.nextDouble();
				employee.add(new SalariedEmployee(name, salary));
			break;
			default: System.out.println("Input was not h or s; please try again.");
			break;
		}
	}

	private static void clearScreen()
        {
		 //Clears the window
       		 System.out.println("\u001b[H\u001b[2J");
        }

}
